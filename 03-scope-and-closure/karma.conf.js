var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('03-scope-and-closure/transpiled/generator-base.js');
  config.files.push('03-scope-and-closure/transpiled/generator-step.js');
  config.files.push('03-scope-and-closure/transpiled/tests.js');

};

