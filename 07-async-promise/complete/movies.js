export class MovieService {
  constructor() {
    this.movies = null;
    this.moviesPromise = null;
  };

  getBestMovies(count) {
    return this.getMovies().then(function(movies) {
      return movies.slice(0, count);
    });
  }

  getMovies() {
    if (!this.moviesPromise) {
      this.moviesPromise = fetchMovies().then((movies) => this.movies = movies);
    }

    return this.moviesPromise;
  }
}

// Načtení dat ze serveru -------------------------------------------------------------------------

function fetchMovies() {
  return fetch("/api/movies").then(function(response) {
    return response.json();
  });
}
