export class ButtonControls {
  constructor(buttonsElement, calculator, display) {

    this.calculator = calculator;
    this.display = display;

    // TODO 2.1 - Reagujte na kliknutí
  }

  processButton(event) {

    // TODO 3 - implementujte reakci na tlačítka
  }

  processOperation_(type) {

    switch (type) {
      case "clear":
        this.calculator.clear();
        this.display.clear();
        break;

      case "equals":
        const result = this.calculator.equals();
        this.display.setValue(result);
        break;

      default:
        this.display.setValue(this.display.getValue());
        this.calculator.setOperand(this.display.getValue());

        this.calculator.setOperation(type);
    }
  }

  processNumber_(value) {
    this.display.addToValue(value);
    this.calculator.setOperand(this.display.getValue());
  }
}
