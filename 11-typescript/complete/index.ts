import { Calculator } from './calculations/calculator'
import { Add, Sub, Mul, Div } from './calculations/operations'
import { ButtonControls } from './ui-components/buttonControls'
import { Display } from './ui-components/display'

export function initCalculator(displayElement: HTMLElement, buttonsElement: HTMLElement) {

  const calculator = new Calculator();
  calculator.addOperation(new Add());
  calculator.addOperation(new Sub());
  calculator.addOperation(new Div());
  calculator.addOperation(new Mul());

  const display = new Display(displayElement);

  const buttonsControl = new ButtonControls(buttonsElement, calculator, display);

}
